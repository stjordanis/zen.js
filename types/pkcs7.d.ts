declare module 'pkcs7' {
    export function pad(plaintext:Uint8Array):Uint8Array
    export function unpad(padded:Uint8Array):Uint8Array
}