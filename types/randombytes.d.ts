declare module 'randombytes' {
    function randomBytes(len:number): Buffer;

    export = randomBytes;
}

