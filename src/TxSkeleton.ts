import {Buffer} from 'buffer'
import {sumBy, isUndefined} from 'lodash'
import {Hash, Mint, Outpoint, Output, Spend} from './Types'
import {getSizeOfVarInt,writeVarInt,readVarInt} from './Serialization'

export class TxSkeleton {
    constructor(
        public inputs:Array<Input>,
        public outputs:Array<Output>) {
    }

    getSize() {
        const inputsSize =
            getSizeOfVarInt(this.inputs.length) +
            sumBy(this.inputs, input => input.getSize())

        const outputsSize =
            getSizeOfVarInt(this.outputs.length) +
            sumBy(this.outputs, output => output.getSize())

        return inputsSize + outputsSize
    }

    write(buffer:Buffer,offset:number) {
        offset = writeVarInt(this.inputs.length, buffer, offset)
        this.inputs.forEach(input => offset = input.write(buffer,offset))

        offset = writeVarInt(this.outputs.length, buffer,offset)
        this.outputs.forEach(output => offset = output.write(buffer,offset))
    }

    static read(buffer:Buffer, offset:number) {
        const {value:inputsLength, offset:tempOffset} = readVarInt(buffer,offset)
        offset = tempOffset

        const inputs:Array<Input> = []
        for (let i = 0; i < inputsLength; i++) {
            const {input,offset:inputOffset} = Input.read(buffer, offset)

            inputs.push(input)

            offset = inputOffset
        }

        const {value:outputsLength, offset:tempOffset2} = readVarInt(buffer,offset)
        offset = tempOffset2

        const outputs:Array<Output> = []
        for (let i = 0; i < outputsLength; i++) {
            const {output,offset:outputOffset} = Output.read(buffer, offset)

            outputs.push(output)

            offset = outputOffset
        }

        const tx = new TxSkeleton(inputs, outputs)

        return {tx, offset}
    }

    serialize() {
        const size = this.getSize()
        const buffer = Buffer.alloc(size)

        this.write(buffer, 0)

        return buffer
    }

    toHex() {
        return this.serialize().toString('hex')
    }

    static fromHex(hex:string) {
        const buffer = Buffer.from(hex,'hex')

        const {tx} = TxSkeleton.read(buffer,0)

        return tx
    }

    toJson() {
        return {
            inputs: this.inputs.map(input => input.toJson()),
            outputs: this.outputs.map(input => input.toJson()),
        }
    }
}

export type PointedOutput = {
    kind:'pointedOutput'
    outpoint:Outpoint,
    output:Output
}

export class Input {
    constructor(public input: PointedOutput | Mint) {

    }

    toJson() {
        switch (this.input.kind) {
            case 'pointedOutput':
                return {
                    kind:'pointedOutput',
                    outpoint:this.input.outpoint,
                    output:this.input.output,
                }
            case 'mint':
                return {
                    kind:'mint',
                    asset: this.input.spend.asset.asset,
                    amount: this.input.spend.amount.toString()
                }
        }
    }

    getSize() {
        switch (this.input.kind) {
            case 'pointedOutput':
                return 1 + this.input.output.getSize() + getSizeOfVarInt(this.input.outpoint.index) + this.input.outpoint.txHash.getSize()
            case 'mint':
                return 1 + this.input.spend.getSize()
        }
    }

    write(buffer:Buffer, offset:number) {
        switch (this.input.kind) {
            case 'pointedOutput':
                offset=buffer.writeUInt8(1,offset)
                offset=this.input.outpoint.txHash.write(buffer,offset)
                offset=writeVarInt(this.input.outpoint.index, buffer, offset)
                return this.input.output.write(buffer, offset)
            case 'mint':
                offset=buffer.writeUInt8(2,offset)
                return this.input.spend.write(buffer,offset)
        }
    }

    static read(buffer:Buffer, offset:number) {
        const identifier = buffer.readUInt8(offset)
        offset++

        switch (identifier) {
            case 1:
                const {hash:txHash,offset:offset2} = Hash.read(buffer,offset)
                const {value:index,offset:offset3} = readVarInt(buffer,offset2)
                const {output:output,offset:offset4} = Output.read(buffer,offset3)

                return {
                    input:new Input({
                        kind: 'pointedOutput',
                        outpoint:{kind: 'outpoint', txHash: txHash, index: index},
                        output:output
                    }), offset:offset4
                }
            case 2:
                const {spend,offset:offset5} = Spend.read(buffer,offset)

                return {
                    input:new Input({
                        kind:'mint',
                        spend:spend
                    }), offset:offset5
                }
            default:
                throw "invalid input"
        }
    }
}