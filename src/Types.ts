import {Buffer} from 'buffer'
import {
    getSizeOfVarInt,
    writeVarInt,
    readVarInt,
    getSizeOfAmount,
    readAmount,
    writeAmount,
    getSizeOfString, writeString, readString
} from './Serialization'
import * as BigInteger from 'bigi'
import {isString, every, findLastIndex, isUndefined} from 'lodash'
import {read as readData} from './Data'

export type Chain = 'local' | 'test' | 'main'

export class Hash {
    bytes:Buffer

    constructor(public hash:string) {
        this.bytes = Buffer.from(this.hash,'hex')
    }

    getSize() {
        return 32
    }

    write(buffer:Buffer,offset:number) {
        this.bytes.copy(buffer,offset)

        return offset+32
    }

    static read(buffer:Buffer,offset:number) {
        const bytes = Buffer.alloc(32)
        buffer.copy(bytes,0, offset)

        const hash = new Hash(bytes.toString('hex'))

        return {hash,offset:offset+32}
    }
}

export class Asset {
    version:number
    cHash:Buffer
    subType:Buffer

    constructor(public asset:string) {
        if (asset === '00') {
            this.version = 0
            this.cHash = Buffer.alloc(32)
            this.cHash.fill(0)
            this.subType = Buffer.alloc(32)
            this.subType.fill(0)
        }
        else {
            const bytes = Buffer.from(asset, 'hex')

            this.version = bytes.readInt32BE(0)
            this.cHash = Buffer.alloc(32)
            bytes.copy(this.cHash,0,4)

            if (bytes.length === 36) {
                this.subType = Buffer.alloc(32)
                this.subType.fill(0)
            }
            else if (bytes.length == 68) {
                this.subType = Buffer.alloc(32)
                bytes.copy(this.subType,0,36)
            }
            else {
                throw 'invalid asset'
            }
        }
    }

    static Zen = new Asset('00')



    static isZero(bytes:Buffer) {
        return every(bytes, b => b === 0)
    }

    versionBytes() {
        const version = this.version

        function byte(x:number) {
            return x & 0xFF
        }

        if ((version & 0xFFFFFFE0) === 0) {
            const b = Buffer.alloc(1)
            b.writeUInt8(version,0)

            return b
        } else if ((version & 0xFFF80000) === 0) {
            const b = Buffer.alloc(3)
            b.writeUInt8(0x20 | byte(version >>> 14),0)
            b.writeUInt8(0x80 | byte(version >>> 7),1)
            b.writeUInt8(0x7F & byte(version) ,2)

            return b
        } else if ((version & 0xFC000000) === 0) {
            const b = Buffer.alloc(4)

            b.writeUInt8(0x20 | byte(version >>> 21),0)
            b.writeUInt8(0x80 | byte(version >>> 14),1)
            b.writeUInt8(0x80 | byte(version >>> 7),2)
            b.writeUInt8(0x7F & byte(version) ,3)

            return b
        } else {
            const b = Buffer.alloc(5)

            b.writeUInt8( 0x20 | byte(version >>> 28),0)
            b.writeUInt8(0x80 | byte(version >>> 21),1)
            b.writeUInt8(0x80 | byte(version >>> 14),2)
            b.writeUInt8(0x80 | byte(version >>> 7),3)
            b.writeUInt8(0x7F & byte(version) ,4)

            return b
        }
    }

    getSize() {
        const versionBytes = this.versionBytes()

        if (Asset.isZero(this.cHash) && Asset.isZero(this.subType)) {
            return versionBytes.length
        } else {
            const index = findLastIndex(this.subType, b=> b !== 0)
            switch (index) {
                case -1:
                    return versionBytes.length + 32
                case 30:
                case 31:
                    return versionBytes.length + 32 + 32
                default:
                    return versionBytes.length + 32 + 1 + index + 1
            }
        }
    }

    write(buffer:Buffer, offset:number) {
        const versionBytes = this.versionBytes()

        if (Asset.isZero(this.cHash) && Asset.isZero(this.subType)) {
            return offset + versionBytes.copy(buffer,offset)
        } else {
            const index = findLastIndex(this.subType, b=> b !== 0)
            switch (index) {
                case -1:
                    versionBytes[0] = 0x80 | versionBytes[0]
                    offset += versionBytes.copy(buffer,offset)
                    offset += this.cHash.copy(buffer,offset)
                    return offset
                case 30:
                case 31:
                    versionBytes[0] = 0xC0 | versionBytes[0]
                    offset += versionBytes.copy(buffer,offset)
                    offset += this.cHash.copy(buffer,offset)
                    offset += this.subType.copy(buffer,offset)

                    return offset
                default:
                    versionBytes[0] = 0x40 | versionBytes[0]
                    offset += versionBytes.copy(buffer,offset)
                    offset += this.cHash.copy(buffer,offset)
                    offset = buffer.writeUInt8(index+1, offset)

                    offset += this.subType.copy(buffer, offset, 0,index+1)

                    return offset
            }
        }
    }

    static readVersion(buffer:Buffer, offset:number, v:number, counter:number) : {version:number,offset:number} {
        if (counter > 3 || (counter !== 0 && v === 0))
            throw 'invalid asset version'

        const b = buffer.readUInt8(offset)
        offset++
        const nextV = v + (b & 0x7F)
        if ((b & 0x80) === 0) {
            return {version:nextV, offset:offset}
        } else {
            return this.readVersion(buffer, offset, nextV,counter+1)
        }
    }

    static read(buffer:Buffer, offset:number) {
        const first = buffer.readUInt8(offset)
        offset++

        let version = first & 0x1F

        if ((first & 0x20) !== 0) {
            const {version:v, offset:offset2} = this.readVersion(buffer,offset,version * 128,0)
            version = v
            offset = offset2
        }

        let cHash = Buffer.alloc(32)

        if ((first & 0xC0) === 0) {
            cHash.fill(0)
        } else {
            offset += buffer.copy(cHash,0,offset)
        }

        let subType = Buffer.alloc(32)

        switch (first & 0xC0) {
            case 0x0:
            case 0x80:
                subType.fill(0)
                break
            case 0xC0:
                offset += buffer.copy(subType,0,offset)
                break
            default:
                const length = buffer.readUInt8(offset)
                offset++
                if (length === 0) {
                    subType.fill(0)
                } else {
                    subType.fill(0)
                    offset += buffer.copy(subType, 0,offset,offset+length)
                }
                break
        }

        if (version === 0 && Asset.isZero(cHash) && Asset.isZero(subType)) {
            return {asset:new Asset('00'),offset:offset}
        } else if (Asset.isZero(subType)) {
            const b = Buffer.alloc(36)
            b.writeUInt32BE(version,0)
            cHash.copy(b,4)

            return {asset:new Asset(b.toString('hex')),offset:offset}
        } else {
            const b = Buffer.alloc(36+32)
            b.writeUInt32BE(version,0)
            cHash.copy(b,4)
            subType.copy(b,36)

            return {asset:new Asset(b.toString('hex')),offset:offset}
        }
    }
}

export class ContractId {
    version:number
    cHash:Buffer

    constructor(public contractId:string) {
        let bytes = Buffer.from(contractId,'hex')

        if (bytes.length !== 36) {
            throw 'invalid contractId'
        } else {
            this.version = bytes.readUInt32BE(0)
            this.cHash = Buffer.alloc(32)
            bytes.copy(this.cHash,0,4)
        }
    }

    toJson() {
        const buffer = Buffer.alloc(36)
        buffer.writeUInt32BE(this.version,0)
        this.cHash.copy(buffer,4)

        return buffer.toString('hex')
    }

    getSize() {
        return getSizeOfVarInt(this.version) + 32
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(this.version, buffer, offset)

        this.cHash.copy(buffer, offset)

        return offset+32
    }

    static read(buffer:Buffer, offset:number) {
        const {value:version,offset:offset2} = readVarInt(buffer,offset)

        const bytes = Buffer.alloc(36)
        bytes.writeUInt32BE(version,0)
        buffer.copy(bytes,1,offset2, offset2 + 36)

        return {contractId:new ContractId(bytes.toString('hex')), offset: offset2+32}
    }
}

export class Spend {
    constructor(public asset:Asset,public amount:BigInteger) {

    }

    toJson() {
        return {
            asset: this.asset.asset,
            amount: this.amount.toString()
        }
    }

    getSize() {
        return this.asset.getSize() + getSizeOfAmount(this.amount)
    }

    write(buffer:Buffer, offset:number) {
        offset = this.asset.write(buffer,offset)
        return writeAmount(this.amount,buffer,offset)
    }

    static read(buffer:Buffer, offset:number) {
        const {asset, offset:offset2} = Asset.read(buffer,offset)
        const {amount,offset:offset3} = readAmount(buffer,offset2)

        return {spend:new Spend(asset,amount),offset:offset3}
    }
}

// Locks
export class Lock {
    toJson() : object {
        throw "please implement method"
    }

    getSize() : number {
        throw "please implement method"
    }

    write(buffer:Buffer, offset:number) : number {
        throw "please implement method"
    }

    static read(buffer:Buffer, offset:number) {
        const {value:identifier,offset:offset2} = readVarInt(buffer,offset)

        switch (identifier) {
            case 2:
                return PKLock.read(buffer,offset2)
            case 1:
                return FeeLock.read(buffer,offset2)
            default:
                const {value:size,offset:offset3} = readVarInt(buffer,offset2)
                const data = Buffer.alloc(size)
                buffer.copy(data, 0,offset3)

                return {lock:new HighVLock(identifier, data), offset:offset3+size}
        }
    }
}
export class PKLock extends Lock {
    constructor(public pkHash:Hash) {
        super()
    }

    toJson() {
        return {
            identifier:2,
            pkHash:this.pkHash.hash
        }
    }

    getSize() {
        return getSizeOfVarInt(2) + getSizeOfVarInt(32) + 32
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(2,buffer,offset)
        offset = writeVarInt(32,buffer,offset)
        return this.pkHash.write(buffer, offset)
    }

    static read(buffer:Buffer, offset:number) {
        const {value:size,offset:offset2} = readVarInt(buffer,offset)
        if (size !== 32) throw "invalid pkLock size"

        const {hash,offset:offset3} = Hash.read(buffer,offset2)

        return {lock:new PKLock(hash), offset:offset3}
    }
}

export class FeeLock extends Lock {

    toJson() {
        return {
            identifier:1
        }
    }

    getSize() {
        return getSizeOfVarInt(1) + getSizeOfVarInt(0)
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(1,buffer,offset)
        offset = writeVarInt(0,buffer,offset)

        return offset
    }

    static read(buffer:Buffer, offset:number) {
        const {value:size,offset:offset2} = readVarInt(buffer,offset)
        if (size !== 0) throw "invalid feelock size"

        return {lock:new FeeLock(),offset:offset2}
    }
}

export class HighVLock extends Lock {
    constructor(public identifier:number, public data:Buffer){
        super()
    }

    toJson() {
        return {
            identifier:this.identifier,
            data:this.data.toString('hex')
        }
    }

    getSize() {
        return getSizeOfVarInt(this.identifier) + getSizeOfVarInt(this.data.length) + this.data.length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(this.identifier,buffer,offset)
        offset = writeVarInt(this.data.length,buffer,offset)

        this.data.copy(buffer,offset)
        offset += this.data.length

        return offset
    }
}

// Witnesses

export class Witness {

    toJson() : object {
        throw "please implement method"
    }

    getSize() :number {
        throw "please implement method"
    }

    write(buffer:Buffer, offset:number) : number {
        throw "please implement method"
    }

    static read(buffer:Buffer, offset:number, parseContractWitness:boolean) {
        const {value:identifier,offset:offset2} = readVarInt(buffer,offset)

        switch (identifier) {
            case 1:
                return PKWitness.read(buffer,offset2)
            case 2:
                if (parseContractWitness)
                    return ContractWitness.read(buffer, offset2)
                // fall to default
            default:
                const {value:size,offset:offset3} = readVarInt(buffer,offset2)
                const data = Buffer.alloc(size)
                buffer.copy(data, 0,offset3)

                return {witness:new HighVWitness(identifier, data), offset:offset3+size}

        }
    }
}

export const SigHashTxHash = 1
export const FollowingWitnesses = 3
export class PKWitness extends Witness {
    constructor(public sigHash:number, public publicKey:Buffer, public signature:Buffer) {
        super()
    }

    toJson() {
        return {
            identifier:1,
            publicKey: this.publicKey.toString('hex'),
            signature: this.signature.toString('hex')
        }
    }

    getSize() {
        let length = this.publicKey.length + this.signature.length + 1

        return getSizeOfVarInt(1) + getSizeOfVarInt(length) + length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(1,buffer,offset)
        offset = writeVarInt(this.publicKey.length + this.signature.length + 1, buffer,offset)
        offset = buffer.writeUInt8(this.sigHash, offset)

        offset = offset + this.publicKey.copy(buffer,offset)
        return offset + this.signature.copy(buffer,offset)
    }

    static read(buffer:Buffer, offset:number) {
        let {value:size,offset:offset2} = readVarInt(buffer,offset)
        offset = offset2

        if (size !== 1 + 33 + 64 ) throw 'invalid pkwitness size'

        const sigHash = buffer.readUInt8(offset)
        offset++

        const publicKey = Buffer.alloc(33)
        const signature = Buffer.alloc(64)

        offset = offset + buffer.copy(publicKey,0,offset)
        offset = offset + buffer.copy(signature,0,offset)

        return {witness:new PKWitness(sigHash, publicKey,signature),offset}
    }
}

type StateCommitmentEnum = 'no-state'| 'not-committed' | Hash

export class StateCommitment {
    constructor(private value:StateCommitmentEnum) {

    }

    getSize() {
        if (isString(this.value)) {
            return 1
        }
        else {
            return 1 + this.value.getSize()
        }
    }

    write(buffer:Buffer, offset:number) {
        if (this.value === 'no-state') {
            buffer.writeInt8(1, offset)
            return offset + 1
        } else if (this.value === 'not-committed') {
            buffer.writeInt8(3, offset)
            return offset + 1
        } else {
            buffer.writeInt8(2, offset)
            return this.value.write(buffer, offset+1)
        }
    }

    static read(buffer:Buffer, offset:number) {
        const identifier = buffer.readInt8(offset)

        switch (identifier) {
            case 1:
                return {stateCommitment: new StateCommitment('no-state'), offset: offset + 1}
            case 2:
                const {hash, offset:offset2} = Hash.read(buffer, offset + 1)
                return {stateCommitment: new StateCommitment(hash), offset: offset2}
            case 3:
                return {stateCommitment: new StateCommitment('not-committed'), offset: offset + 1}
        }

        throw 'invalid state-commitment identifier'
    }
}


export class ContractWitness extends Witness {
    constructor(public contractId:ContractId,
                public command:string,
                public stateCommitment:StateCommitment,
                public beginInputs: number,
                public beginOutputs: number,
                public inputsLength: number,
                public outputsLength: number,
                public cost:Buffer,
                public messageBody?:any,
                public signature?: [Buffer,Buffer]) {
        super()
    }

    toJson() {
        return {
            identifier: 2,
            contractId: this.contractId.toJson(),
            command: this.command,
            stateCommitment: this.stateCommitment.toString(),
            beginInputs: this.beginInputs,
            beginOutputs: this.beginOutputs,
            inputsLength: this.inputsLength,
            outputsLength: this.outputsLength,
            cost: this.cost,
            messageBody: this.messageBody,
            signature: this.signature
        }
    }

    getPayloadSize() {
        return this.contractId.getSize() +
            getSizeOfString(this.command) +
            1 + (isUndefined(this.messageBody) ? 0 : this.messageBody.getSize()) +
            this.stateCommitment.getSize() +
            getSizeOfVarInt(this.beginInputs) +
            getSizeOfVarInt(this.beginOutputs) +
            getSizeOfVarInt(this.inputsLength) +
            getSizeOfVarInt(this.outputsLength) +
            1 + (isUndefined(this.signature) ? 0 : this.signature[0].length + this.signature[1].length)
            + 8 // cost
    }

    getSize() {
        let length = this.getPayloadSize()

        return getSizeOfVarInt(2) + getSizeOfVarInt(length) + length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(2, buffer, offset)
        offset = writeVarInt(this.getPayloadSize(), buffer, offset)
        offset = this.contractId.write(buffer, offset)
        offset = writeString(this.command, buffer, offset)

        if (isUndefined(this.messageBody)) {
            offset = buffer.writeUInt8(0, offset)
        } else {
            offset = buffer.writeUInt8(1, offset)
            offset = this.messageBody.write(buffer, offset)
        }

        offset = this.stateCommitment.write(buffer, offset)
        offset = writeVarInt(this.beginInputs, buffer, offset)
        offset = writeVarInt(this.beginOutputs, buffer, offset)
        offset = writeVarInt(this.inputsLength, buffer, offset)
        offset = writeVarInt(this.outputsLength, buffer, offset)

        if (isUndefined(this.signature)) {
            offset = buffer.writeUInt8(0, offset)
        } else {
            offset = buffer.writeUInt8(1, offset)
            offset = offset + this.signature[0].copy(buffer, offset)
            offset = offset + this.signature[1].copy(buffer, offset)
        }

        offset = offset + this.cost.copy(buffer, offset)

        return offset
    }

    static read(buffer:Buffer, offset:number) {
        const {offset:offset1} = readVarInt(buffer, offset)
        offset = offset1
        const {contractId, offset: offset2} = ContractId.read(buffer, offset)
        const {value:command, offset:offset3} = readString(buffer, offset2)

        let messageBody = undefined
        const isMessageBody = buffer.readInt8(offset3)
        offset = offset3 + 1

        if (isMessageBody === 1) {
            const {data, offset: offset4} = readData(buffer, offset)
            offset = offset4
            messageBody = data
        }

        const {stateCommitment, offset: offset5} = StateCommitment.read(buffer, offset)
        const {value:beginInputs, offset: offset6} = readVarInt(buffer, offset5)
        const {value:beginOutputs, offset: offset7} = readVarInt(buffer, offset6)
        const {value:inputsLength, offset: offset8} = readVarInt(buffer, offset7)
        const {value:outputsLength, offset: offset9} = readVarInt(buffer, offset8)

        let signaturePair:[Buffer,Buffer] | undefined = undefined
        const isSignature = buffer.readInt8(offset9)
        offset = offset9 + 1

        if (isSignature === 1) {
            const publicKey = buffer.slice(offset, offset + 33)
            offset += 33
            const signature = buffer.slice(offset, offset + 64)
            offset += 64
            signaturePair = [publicKey, signature]
        }

        const cost = buffer.slice(offset, offset + 8)
        offset += 8

        return {witness:new ContractWitness(
                contractId,
                command,
                stateCommitment,
                beginInputs,
                beginOutputs,
                inputsLength,
                outputsLength,
                cost,
                messageBody,
                signaturePair
            ), offset}
    }
}

export class HighVWitness extends Witness {
    constructor(public identifier:number, public data:Buffer) {
        super()
    }

    toJson() {
        return {
            identifier:this.identifier,
            data:this.data.toString('hex')
        }
    }

    getSize() {
        let length = this.data.length

        return getSizeOfVarInt(1) + getSizeOfVarInt(length) + length
    }

    write(buffer:Buffer, offset:number) {
        offset = writeVarInt(this.identifier,buffer,offset)
        offset = writeVarInt(this.data.length, buffer, offset)

        return offset + this.data.copy(buffer,offset)
    }
}

// Input

export interface Outpoint {
    kind:'outpoint'
    txHash:Hash,
    index:number
}

export interface Mint {
    kind:'mint'
    spend:Spend
}

export class Input {
    constructor(public input: Outpoint | Mint) {

    }

    toJson() {
        switch (this.input.kind) {
            case 'outpoint':
                return {
                    kind:'outpoint',
                    txHash:this.input.txHash.hash,
                    index:this.input.index
                }
            case 'mint':
                return {
                    kind:'mint',
                    asset: this.input.spend.asset.asset,
                    amount: this.input.spend.amount.toString()
                }
        }
    }

    getSize() {
        switch (this.input.kind) {
            case 'outpoint':
                return 1 + getSizeOfVarInt(this.input.index) + this.input.txHash.getSize()
            case 'mint':
                return 1 + this.input.spend.getSize()
        }
    }

    write(buffer:Buffer, offset:number) {
        switch (this.input.kind) {
            case 'outpoint':
                offset=buffer.writeUInt8(1,offset)
                offset=this.input.txHash.write(buffer,offset)
                return writeVarInt(this.input.index, buffer, offset)
            case 'mint':
                offset=buffer.writeUInt8(2,offset)
                return this.input.spend.write(buffer,offset)
        }
    }

    static read(buffer:Buffer, offset:number) {
        const identifier = buffer.readUInt8(offset)
        offset++

        switch (identifier) {
            case 1:
                const {hash:txHash,offset:offset2} = Hash.read(buffer,offset)
                const {value:index,offset:offset3} = readVarInt(buffer, offset2)

                return {
                    input:new Input({
                        kind:'outpoint',
                        txHash:txHash,
                        index:index
                    }), offset:offset3}
            case 2:
                const {spend,offset:offset4} = Spend.read(buffer,offset)

                return {
                    input:new Input({
                        kind:'mint',
                        spend:spend
                    }), offset:offset4}
            default:
                throw "invalid input"
        }
    }
}

// Output

export class Output {
    constructor (public lock:Lock,public spend:Spend) {

    }

    toJson() {
        return {
            lock:this.lock.toJson(),
            spend:this.spend.toJson()
        }
    }

    getSize() {
        return this.lock.getSize() + this.spend.getSize()
    }

    write(buffer:Buffer,offset:number) {
        offset =this.lock.write(buffer,offset)
        return this.spend.write(buffer,offset)
    }

    static read(buffer:Buffer,offset:number) {
        const {lock,offset:offset2} = Lock.read(buffer,offset)
        const {spend,offset:offset3} = Spend.read(buffer,offset2)

        return {output:new Output(lock,spend),offset:offset3}
    }
}

// Contracts

export class Contract {
    constructor(public version:number, public data:Buffer) {
    }

    toJson() {
        return {
            version:this.version,
            data:this.data.toString('hex')
        }
    }

    getSize() {
        return getSizeOfVarInt(this.version) + getSizeOfVarInt(this.data.length) + this.data.length
    }

    write(buffer:Buffer,offset:number) {
        offset = writeVarInt(this.version,buffer,offset)
        offset = writeVarInt(this.data.length, buffer,offset)

        return offset + this.data.copy(buffer, offset)
    }

    static read(buffer:Buffer,offset:number) {
        const {value:version,offset:offset2} = readVarInt(buffer, offset)
        const {value:size, offset:offset3} = readVarInt(buffer, offset2)

        const data = Buffer.alloc(size)

        const offset4 = offset3 + buffer.copy(data, 0, offset2)

        return {contract: new Contract(version,data),offset:offset4 }
    }
}

