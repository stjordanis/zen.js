Zen.JS
========

Zen.JS is javascript library for Zen Protocol.

# Installation

```
npm config set @zen:registry https://www.myget.org/F/zenprotocol/npm/
npm install --save @zen/zenjs
```

# Generating mnemonic phrase, keys and accepting payments
```
import {Mnemonic, ExtendedKey} from '@zen/zenjs'

const mnemonic = Mnemonic.generateMnemonic(24);
const extendedKey = ExtendedKey.fromMnemonic(mnemonic);

const privateKey = extendedKey.derivePath("m/44'/258'/0'/0/0").getPrivateKey();
const publicKey = extendedKey.getPublicKey();

console.log(publicKey.toAddress());
```

# Creating and signing transactions

```
import {TransactionBuilder,ExtendedKey} from '@zen/zenjs'
import {post} from 'axios'

const mnemonic = 'one one one one one one one one one one one one one one one one one one one one one one one one';
const privateKey = ExtendedKey.fromMnemonic(mnemonic).derivePath("m/44'/258'/0'/0/0").getPrivateKey();

const tb = new TransactionBuilder('test');
tb.addInput('0000000000000000000000000000000000000000000000000000000000000000',0, privateKey);
tb.addOutput('tp1qfyplhxql09lvvg53dxg7t77tkkxhsp3l6q8xjjpj85hvqlw0ttqswjdapx', 100, '00');

const tx = tb.sign();

console.log(tx.hash());
console.log(tx.toJson());
const hex = tx.toHex();

// Transaction is ready to be published
post('http://127.0.0.1/:31567/blockchain/publishtransaction',hex,{ headers: { 'Content-Type': 'application/json' }});
```

# Testing

Setup a running node:

```
./zen-node --localhost --addressdb --wipe full
./zen-cli import 1234 feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch thought
./zen-cli resync
./zen-cli activate ~/zenprotocol/src/ContractExamples/Token.fst 100 1234
```

Launch a remote-node (which defaults to use the node above):

```
zen-remote-node$ npm run dev
```

Run the tests:

```
npm run test
```
