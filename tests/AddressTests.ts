import {Address} from '../index'
import { expect } from 'chai'
import {Hash, Chain, ContractId} from '../index'


describe('Address tests', () => {
    it('can get an address from pkhash', () => {

        const address = Address.getPublicKeyHashAddress('main', 'd140fb570bfbf23a3ff3c63ee39e942bd4374e15804d0e0461f1583448e644ab')
    })
    it('can decode Address', () => {

        const {hash:hash} = Address.decode('test', 'tzn1q4l4zu4ymc5hq2eenjctpu96m4z6ant72whr93mpuguulcnlwegpq5qrary') as Hash
        
        const fsValue = 'afea2e549bc52e05673396161e175ba8b5d9afca75c658ec3c4739fc4feeca02'
        expect(hash).to.be.equal(fsValue)
    })
    it('can decode contracts', () => {

        const {contractId: hash} = Address.decode('test', 'ctzn1qqqqqqqxe34sja4nxzgvmspehe9v0uqm03rwh05ufu24rhuk65vcx9ja8yv7f5sjc') as ContractId
        
        const fsValue = '00000000d98d612ed6661219b80737c958fe036f88dd77d389e2aa3bf2daa33062cba723'
        expect(hash).to.be.equal(fsValue)
    })
})