import {TransactionBuilder,PKLock, Hash,ExtendedKey, Transaction} from '../index'
import * as util from 'util'
import { expect } from 'chai'
import 'mocha'

describe('signing transaction', () => {

    it('should equal signing from zen-node', () => {
        const mnemonic = 'one one one one one one one one one one one one one one one one one one one one one one one one'
        const privateKey = ExtendedKey.fromMnemonic(mnemonic).derivePath("m/44'/258'/0'/0/0").getPrivateKey()

        const tb = new TransactionBuilder('test')
        tb.addInput(
            '0000000000000000000000000000000000000000000000000000000000000000',0, privateKey)

        tb.addOutput(
            new Hash('0000000000000000000000000000000000000000000000000000000000000000'),
            100,
            '00')

        const tx = tb.sign()

        // console.log(util.inspect( tx.toJson(), {
        //     depth:null
        // }))

        const hex = tx.toHex()
        const txHash = tx.hash().hash

        expect(txHash).to.equal('2df00d7cf448facbe9883a032dd435b08c74e40a18c9f91c1a43be583ea3ce4a');
        expect(hex).to.equal(
            '0000000001010000000000000000000000000000000000000000000000000000000000000000000102200000000000000000000000000000000000000000000000000000000000000000000801000101620102697490c7e4b75a92812d0508dbbf978c849dcba90178a8da822318688cc3b357f8c1866b7e9296b6df1ae3eed8c75e0ddae52b332da8d036f3344f05f3cf0c7f3ad2bb054f412a55039a952153166e7044e4e07fffa6e7b493d629a1d4b70a14')

        const tx2 = Transaction.fromHex(hex)

        // console.log(util.inspect( tx2.toJson(), {
        //     depth:null
        // }))

        expect(tx2.hash().hash).to.equals(txHash)
        expect(tx2.toHex()).to.equals(hex)
    });

});