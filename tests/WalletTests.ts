import {get} from 'lodash'
import {Wallet} from '../src/Wallet'
import {expect} from "chai";
import {Data} from '../src/Data';

const phraseSender =    'feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch thought'
const phraseReceiver =  'feel muffin volcano click mercy abuse bachelor ginger limb tomorrow okay input spend athlete boring security document exclude liar dune usage camera ranch one'
const chain =           'local'
const remoteNode =      'http://localhost:3000'
const ZEN =             '00'
const contractAddress = 'ctzn1qqqqqqqp6rr4wllugtwn96juhdh8gasy4gwkakhz7n0ajcffy7yemfpk9yy0jr2f4'
const contractId =      '000000003a18eaefff885ba65d4b976dce8ec09543addb5c5e9bfb2c2524f133b486c521'

let getZenBalance = async (wallet:Wallet) => {
    await wallet.refresh()
    return get(wallet.getBalance(),ZEN)
}

let initSenderWallet = () => {
    return Wallet.fromMnemonic(phraseSender, chain, new Wallet.RemoteNodeWalletActions(remoteNode))
}

let initReceiverWallet = () => {
    return Wallet.fromMnemonic(phraseReceiver, chain, new Wallet.RemoteNodeWalletActions(remoteNode))
}

let getBalances = async () => {
    return {
        sender: await getZenBalance(initSenderWallet()),
        receiver: await getZenBalance(initReceiverWallet())
    }
}

let send = async (asset:string, sentAmount = Math.floor(Math.random() * 1000) + 100) => {
    const wallet = initSenderWallet()
    await wallet.refresh()
    await wallet.send([{
        address:initReceiverWallet().getAddress(),
        asset:asset,
        amount:sentAmount
    }])
    return sentAmount
}

let expectTx = async (senderWallet:Wallet, receiverWallet:Wallet, asset:string, amount:number) => {
    expect((await senderWallet.getTransactions()).filter((tx:Wallet.TransactionInfo) => {
        return tx.asset === asset && tx.amount === -1 * amount && tx.confirmations === 0
    }), "sender should have tx").to.have.length.of.at.least(1)

    expect((await receiverWallet.getTransactions()).filter((tx:Wallet.TransactionInfo) => {
        return tx.asset === asset && tx.amount === amount && tx.confirmations === 0
    }), "receiver should have tx").to.have.length.of.at.least(1)
}

describe('Wallet', () => {
    it('should get balances', getBalances)
    it('should send ZEN', async () => send(ZEN))
    it('should update balances', async () => {
        const balances = await getBalances()
        const sentAmount = await send(ZEN)
        const newBalances = await getBalances()
        expect(newBalances.sender).to.be.equals(balances.sender - sentAmount)
        expect(newBalances.receiver).to.be.equals(balances.receiver + sentAmount)
    })
    it('should show transactions', async () => {
        const sentAmount = await send(ZEN)
        await expectTx(await initSenderWallet(), await initReceiverWallet(), '00', sentAmount)
    })
    it('should execute contract', async () => {
        const wallet = await initSenderWallet()
        await wallet.refresh()
        await wallet.executeContract(contractAddress, "buy",new Data.Dictionary([]), true, [{
            address:wallet.getAddress(),
            asset:ZEN,
            amount:10
        }])
    })
    it('should send non ZEN asset', async () => {
        const sentAmount = await send(contractId, Math.floor(Math.random() * 10) + 1)
        await expectTx(await initSenderWallet(), await initReceiverWallet(), contractId, sentAmount)
    })
})
