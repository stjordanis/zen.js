import {fromYaml, serialize, read, Data} from '../src/Data'
import { expect } from 'chai'
import BigInteger = require('bigi')
import {ExtendedKey} from '../src/Crypto'
import {sha3_256} from 'js-sha3'

function checkDictionaryType (object:any, type:any){
    expect(object[1].constructor.name).to.be.equal(type)
}
function checkArrayType (object:any, type:any){
    expect(object.constructor.name).to.be.equal(type)
}
function checkValue (data:any ,string:string){
    const{value} = data
    expect(value.intValue()).to.be.equal(new BigInteger(string,10,undefined).intValue())
}

describe('Zen.Data', () => {
    it('should parse yaml', () => {

        const yaml =
            'text: hello\n' +
            'returnAddress: zen1q03jc77dtd2x2gk90f40p9ezv5pf3e2wm5hy8me2xuxzmjneachrq6g05w5\n' +
            'amount: 1.1ZP\n' +
            'uint64: 111UL\n' +
            'int64: 5\n' +
            'hash: dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990e\n' +
            'array:\n' +
            '- dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990e\n' +
            '- dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990eP\n' +
            '- dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990e1\n' +
            '- dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990E\n' +
            '- dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990z'

        const data = fromYaml('main',yaml)
        const [text, returnAddress, amount, uint64, int64, hash, array] = data.data

        checkDictionaryType(text, 'String')
        checkDictionaryType(returnAddress, 'PKAddress')
        checkDictionaryType(amount, 'UInt64')
        checkDictionaryType(uint64, 'UInt64')
        checkDictionaryType(int64, 'Int64')
        checkDictionaryType(hash, 'Hash')
        checkDictionaryType(array, 'Array')

        checkArrayType(array[1][0],'Hash')
        checkArrayType(array[1][1],'String')
        checkArrayType(array[1][2],'String')
        checkArrayType(array[1][3],'String')
        checkArrayType(array[1][4],'String')

        expect(serialize(data)).to.be.equal('0c07046861736807dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990e0474657874060568656c6c6f0561727261790d0507dc411df81bc299131defe7d1ec42ce4670a8e53f2930cc8300ba4314d830990e064164633431316466383162633239393133316465666537643165633432636534363730613865353366323933306363383330306261343331346438333039393065500641646334313164663831626332393931333164656665376431656334326365343637306138653533663239333063633833303062613433313464383330393930653106406463343131646638316263323939313331646566653764316563343263653436373061386535336632393330636338333030626134333134643833303939304506406463343131646638316263323939313331646566653764316563343263653436373061386535336632393330636338333030626134333134643833303939307a05696e74363401000000000000000506616d6f756e740500000000068e77800675696e74363405000000000000006f0d72657475726e416464726573730802207c658f79ab6a8ca458af4d5e12e44ca0531ca9dba5c87de546e185b94f3dc5c6')
    })

    it('should parse 50zp correctly', () => {
        const yaml = '50ZP'
        const data = fromYaml('main', yaml)

        checkArrayType(data, 'UInt64')
        expect(serialize(data)).to.be.equal('05000000012a05f200')
    })
    it('should parse UInt64' ,() =>{
        const yaml = '50UL'
        const data = fromYaml('main', yaml)

        checkArrayType(data,'UInt64')
        checkValue(data,'50')
    })
    it('should not parse signed UInt64' ,() =>{
        const yaml = '-50UL'
        const data = fromYaml('main', yaml)

        checkArrayType(data,'String')
    })
    it('should parse Int64 ' ,() =>{
        const yaml = '-50'
        const data = fromYaml('main', yaml)

        checkArrayType(data,'Int64')
        checkValue(data,'-50')
    })
    it('should parse UInt32' ,() =>{
        const yaml = '50ul'
        const data = fromYaml('main', yaml)

        checkArrayType(data,'UInt32')
        checkValue(data,'50')
    })
    it('should not parse signed UInt32' ,() =>{
        const yaml = '-50ul'
        const data = fromYaml('main', yaml)

        checkArrayType(data,'String')
    })
    it('should parse pk', () => {
        const yaml = 'pk: !pk 03764f6e5b31ba26f94e96652d9e53d396cdb04678aa4407a5213adf34469c6166'
        const data = fromYaml('main', yaml)
        const [pk] = data.data

        checkDictionaryType(pk, "PublicKey")
    })
    it('should serialize and deserialize', () => {
        const yaml = 'UnderlyingAsset: 0000000004d2676b5d414d98e1b69289e9f707531114dcf707587611ef56b1e59ca66623446f726f6e000000000000000000000000000000000000000000000000000000\n' +
            'UnderlyingAmount: 50UL\n' +
            'PairAsset: 00\n' +
            'OrderTotal: 100000000UL\n' +
            'MakerPubKey: !pk 03764f6e5b31ba26f94e96652d9e53d396cdb04678aa4407a5213adf34469c6166'
        const data = fromYaml('main', yaml)
        const hex = serialize(data)

        const {data:data2} = read(Buffer.from(hex, 'hex'), 0)

        const hex2 = serialize(data2)

        expect(hex2).to.be.equal(hex)
    })
    it('serialization and deserialize round-trip of signature should yield same value', () => {
        const extKey = ExtendedKey.fromMnemonic("one one one one one one one one one one one one one one one one one one one one one one one one")
        const messageHash = Buffer.from(sha3_256("message"), 'hex')
        const signature = extKey.getPrivateKey().sign(messageHash)

        const signatureData = new Data.Signature(signature)

        const serializedSignatureData = Data.serialize(signatureData)

        /*
        [<Test>]
        let ``Typescript testvector``() =
            let privateKey =
                "one one one one one one one one one one one one one one one one one one one one one one one one"
                |> ExtendedKey.fromMnemonicPhrase
                >>= ExtendedKey.getPrivateKey
                |> get

            "message"
            |> System.Text.Encoding.ASCII.GetBytes
            |> Hash.compute
            |> Crypto.sign privateKey
            |> ZFStar.fsToFstSignature
            |> Zen.Types.Data.Signature
            |> Serialization.Data.serialize
            |> FsBech32.Base16.encode
            |> printfn "%A" // "0912aa20793a364e62ac82c5462fb977a0219ff2b3d87f1e4c61bb4978f8c3ffc469248aee44d1d10bb4df24675896ee54d969d9420782f56ecb00e4544012cb45"
        */
        
        const fsValue = "0912aa20793a364e62ac82c5462fb977a0219ff2b3d87f1e4c61bb4978f8c3ffc469248aee44d1d10bb4df24675896ee54d969d9420782f56ecb00e4544012cb45"

        expect(serializedSignatureData).to.be.equal(fsValue)
        
        const {data:deserializedSignatureData} = Data.read(Buffer.from(serializedSignatureData,'hex'), 0)

        expect(Data.serialize(deserializedSignatureData)).to.be.equal(serializedSignatureData)
        
        
    })
})
